<?php

include('../../rutas/route.php');

//recogiendo variables desde la URL con GET

$id = $_GET['id'];

$obj =  new TramiteController();
$result = $obj->mostrar($id);

$tramite = $result->fetch_object();
//print_r($tramite);


?>


<?php

include('../templates/app.php');


?>


	
<div class="container">
	

	<div class="row"></div>

	<div class="col-sm-2"></div>

	<div class="col-sm-8">
		<h1>Detalle</h1>
		<hr>

		<ul class="list_group">
			<li class="list_group-item"> <strong>Id</strong> <?php echo $tramite->id ?></li>
			<li class="list_group-item"> <strong>Numero</strong> <?php echo $tramite->numero ?> </li>
			<li class="list_group-item"> <strong>Nombre</strong> <?php echo $tramite->nombre ?> </li>
		</ul>

		<hr>
		<a href="./" class="btn btn-lg btn-default">Volver</a>

	</div>

	<div class="col-sm-2"></div>



	</div>
	
</div>

<?php

include('../templates/footer.php');

?>
