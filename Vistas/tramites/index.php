


<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Lista de Tramites</title>
	<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body>
	
<!-- agregamos no imprimir en esta parte 18/08/2018 -->
<div class="container">
<div class="noImprimir">
	<h1>Lista de Tramites</h1>
	<hr>
	<button class="btn btn-success">Aceptar</button>
	<button class="btn btn-danger">Eliminar</button>
	<button class="btn btn-warning">Inactivar</button>
	<button type="button" class="btn btn-primary">Activar</button>
	<button type="button" class="btn btn-dark">Dark</button>
	</div>
<hr>

<p class="text-right noImprimir">
<a href="create.php" class="btn btn-lg btn-primary">
<span class="glyphicon glyphicon-plus"></span>
Nuevo Tramite
</a>

</p>

<div class="panel panel-default">
  <!-- Default panel contents -->
  <div class="panel-heading noImprimir">Lista</div>
  
  <?php include('table.php')?>
 


</div>


</body>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"/>
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>





</html>