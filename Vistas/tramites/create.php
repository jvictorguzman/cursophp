
<?php
include('../templates/app.php')?>
<div class="container">

<hr>

<div class="row">
<div class="col sm-12 text-right">
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#pnlNuevo">Nuevo</button>
</div>

<div class="col-sm-2"></div>
<div class="col-sm-8">


<!--inicio de modal 18/08/2018--> 

<!-- Modal -->
<div class="modal fade" id="pnlNuevo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Nuevo</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

      <?php include('fields.php')?><!--incluimos para el modal 18/08/2018-->  
      
      </div>
      
    </div>
  </div>
</div>




<!--fin de modal 18/08/2018--> 

<div id="result"></div>
<div id="grilla"></div>


</div>

<div class="col-sm-2"></div>
</div>
</div>




<?php include('../templates/footer.php')?>
<!-- codigo java script con jquery-->
<script type="text/javascript">

$(document).ready(function(){
 //cargando la grilla en el div grilla
 $("#grilla").load("table.php");


//click en btnGuardar
$("#frmDatos").submit(function(event){

event.preventDefault();
    
    $.ajax({
        type: "POST",
        url: "store.php",
        data: $("#frmDatos").serialize(),
        success: function(res){
            console.log(res);
            if($.trim(res) === "ok")
            {      
                //cerrando el modal
                $("#pnlNuevo").modal("hide");      
                $("#result").html("se inserto correctamente");
                
                //cargando la grilla en el div grilla
                $("#grilla").load("table.php");
            }
            else
                $("#result").html(res);    

        }
    });
    
});

});

</script>

