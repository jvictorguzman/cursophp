<?php 

include('../../rutas/route.php');


$obj = new TramiteController();
$tramites = $obj->listar();
//print_r($tramites);

?>
<!--añadiendo hojas de estilo css 18/08/2018 -->
<!--se añade finalmente el media=print el cual permite solo ocultar al moemtno de la impresion -->
<style type="text/css">
.encabezado{
	display:none;
}
</style>


<link rel="stylesheet" href="../css/print.css" media="print">
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.12.1/bootstrap-table.min.js"></script>
</head>
<body>

<div class="encabezado">
	<h2>Reporte de Tramites</h2>
	<hr>
</div>

<p class="text-center noImprimir"><!--se agrego no impirmir en el boton imprimir para ocultarlo 18/08/2018 -->
<button class="btn btn-info" onclick="window.print()">Imprimir</button>
</p>

<!-- esta parte es para agregar buscador, paginacion-->
	<table data-toggle="table"
			data-search="true" 
			data-pagination="true"
			data-show-refresh="true"
			data-show-toggle="true"
			data-show-columns="true">
	
	<thead>
        <tr>

    	<th data-sortable="true">Id</th>
    	<th data-sortable="true">Numero</th>
    	<th data-sortable="true">Nombre</th>
    	<th class= "noImprimir">Opciones</th> <!--se agrego no impirmir para ocultar el th opciones 18/08/2018 -->
	    </tr>
	</thead>
	<tbody>
      
	<?php while($row = $tramites->fetch_object()): ?>

	<tr>
		<td> <?php echo $row->id  ?> </td>
		<td> <?php echo $row->numero ?> </td>
		<td> <?php echo $row->nombre ?> </td>

		<td class= "noImprimir"><!--se agrego no impirmir para ocultar el modificar,eliminar y ver 18/08/2018 -->
			<a href="edit.php? id=<?php echo $row->id ?>" class="btn btn-sm btn-info"><span class="glyphicon glyphicon-pencil"></span>Modificar</a> 
			<a href="eliminar.php? id=<?php echo $row->id ?>"" class = "btn btn-sm btn-danger" onclick="return confirm('Desea Eliminar?')">
            <span class="glyphicon glyphicon-remove"></span>Eliminar</a>	
            <a href="show.php? id=<?php echo $row->id ?>" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-eye-close"></span>Ver</a>

			

            		
          

		</td>

	</tr>

	<?php endwhile; ?>
	</tbody>

</table>