<?php

include('../../rutas/route.php');

$id = $_GET['id'];

$obj = new TramiteController();
if($obj->eliminar($id)):

?>

<?php include('../templates/app.php') ?>


<div class="container">

<p class="alter alert-success">Registro eliminado correctamente</p>

<?php else: ?> 

<p class="alter alter-danger">Ha ocurrido un error, vuelva a intentarlo</p>

<?php endif; ?>

<button class= "btn btn-lg btn-primary" onclick="history.back(-1)">Volver.....</a>


</div>

<?php include('../templates/footer.php'); ?>

 